import { Client, resources } from 'coinbase-commerce-node';
import { Router } from 'express';
import { ACCEPTED, INTERNAL_SERVER_ERROR } from 'http-status-codes';

const coinbaseRouter = Router();

coinbaseRouter.post('/charge', async (req, res) => {
	const coinbaseClient = Client;
	/**
	 * Init Coinbase client
	 * @docs {@link https://github.com/coinbase/coinbase-commerce-node#usage}
	 */
	coinbaseClient.init(process.env.COINBASE_KEY);
	/**
	 * Create product
	 * @docs {@link https://github.com/coinbase/coinbase-commerce-node#create}
	 */
	const chargeObj = new resources.Charge({
		name: req.body.name,
		description: req.body.description,
		local_price: {
			amount: req.body.price.toString(),
			currency: 'USD'
		},
		pricing_type: 'fixed_price'
	});

	try {
		const response = await chargeObj.save();
		return res.status(ACCEPTED).json(response);
	} catch (error) {
		return res.status(INTERNAL_SERVER_ERROR).json({
			message: error.message
		});
	}
});

export default coinbaseRouter;
