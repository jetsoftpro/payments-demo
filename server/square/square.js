import { randomBytes } from 'crypto';
import { Router } from 'express';
import { ACCEPTED, INTERNAL_SERVER_ERROR } from 'http-status-codes';
import { ApiClient, PaymentsApi } from 'square-connect';

const squareRouter = Router();

squareRouter.post('/charge', async (req, res) => {
	const { nonce, price, token } = req.body,
		idempotencyKey = randomBytes(22).toString('hex');
	ApiClient.instance.authentications['oauth2'].accessToken = process.env.SQUARE_ACCESS_TOKEN;
	if (!process.env.IS_PROD) {
		// Set sandbox url
		ApiClient.instance.basePath = 'https://connect.squareupsandbox.com';
	}
	// Charge the customer's card
	const payments_api = new PaymentsApi(null);
	const paymentInfo = {
		source_id: nonce,
		amount_money: {
			amount: price * 100,
			currency: 'USD'
		},
		idempotency_key: idempotencyKey
	};

	try {
		const response = await payments_api.createPayment(paymentInfo);
		res.status(ACCEPTED).json(response.payment);
	} catch ({ response: { text } }) {
		const message = JSON.parse(text).errors[0].detail;
		res.status(INTERNAL_SERVER_ERROR).json({
			message: message
		});
	}
});

export default squareRouter;
