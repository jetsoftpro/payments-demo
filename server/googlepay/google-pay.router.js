import { Router } from 'express';
import { ACCEPTED, INTERNAL_SERVER_ERROR } from 'http-status-codes';

const googlePayRouter = Router();

googlePayRouter.post('/charge', async (req, res) => {
	if (Math.random() >= 0.5) {
		return res.send(ACCEPTED);
	} else {
		return res.status(INTERNAL_SERVER_ERROR).json({
			message: 'Internal server error'
		});
	}

});

export default googlePayRouter;
