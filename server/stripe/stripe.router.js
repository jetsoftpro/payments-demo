import { Router } from 'express';
import { ACCEPTED, INTERNAL_SERVER_ERROR } from 'http-status-codes';
import Stripe from 'stripe';

const stripeRouter = Router();

stripeRouter.post('/charge', async (req, res) => {
	try {
		//stripe instance
		/**
		 * @docs {@link https://stripe.com/docs/api/authentication}
		 */
		const privateKey = process.env.STRIPE_SECRET_KEY,
			stripe = new Stripe(privateKey, {
				apiVersion: '2020-03-02'
			});

		//stripe logic
		const { price, source } = req.body,
			/**
			 * @docs {@link https://stripe.com/docs/api/payment_intents/create}
			 */
			paymentIntent = await stripe.paymentIntents.create({
				// amount: _stripeConvertFromCents(price),
				currency: 'usd',
				source: source,
				setup_future_usage: 'on_session',
				payment_method_options: {
					card: {
						request_three_d_secure: 'any'
					}
				}
			});
		return res.status(ACCEPTED).json({
			secret: paymentIntent.client_secret
		});
	} catch (error) {
		return res.status(INTERNAL_SERVER_ERROR).json({
			message: error.raw.message
		});
	}
});

const _stripeConvertFromCents = (price) => price * 100;

export default stripeRouter;

