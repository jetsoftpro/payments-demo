import bodyParser from 'body-parser';
import cors from 'cors';
import { config } from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import path from 'path';

import braintreeRouter from './braintree/braintree.router';
import coinbaseRouter from './coinbase/coinbase.router';
import googlePayRouter from './googlepay/google-pay.router';
import squareRouter from './square/square';
import stripeRouter from './stripe/stripe.router';

const PORT = process.env.PORT;

/**
 * Environment variables config
 */
config({
	path: path.resolve(process.cwd(), '.env'),
	encoding: 'latin1'
});

/**
 * App instance
 */
const app = express();

/**
 * Middleware for json parsing
 */
app.use(bodyParser.json());

/**
 * Cors middleware
 */
app.use(cors());

/**
 * Logging middleware
 */
app.use(morgan('dev'));

/**
 * Stripe routes
 */
app.use('/stripe', stripeRouter);

/**
 * Braintree routes
 */

app.use('/braintree', braintreeRouter);

/**
 * Google Pay
 */
app.use('/googlepay', googlePayRouter);

/**
 * CoinBase
 */
app.use('/coinbase', coinbaseRouter);

/**
 * Square
 */
app.use('/square', squareRouter);

/**
 * Listen server
 */
app.listen(PORT, () => {
	console.log(`Example app listening on port ${PORT}!`);
});
