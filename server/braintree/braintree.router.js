import braintree from 'braintree';
import { Router } from 'express';
import { ACCEPTED, INTERNAL_SERVER_ERROR } from 'http-status-codes';

const braintreeRouter = Router();

const getGateway = () => {
	const merchantID = process.env.BRAINTREE_MERCHANT_ID;
	const publicKey = process.env.BRAINTREE_PUBLIC_KEY;
	const privateKey = process.env.BRAINTREE_PRIVATE_KEY;
	return braintree.connect({
		environment: braintree.Environment.Sandbox,
		merchantId: merchantID,
		publicKey: publicKey,
		privateKey: privateKey
	});
};

braintreeRouter.get('/client_token', async (req, res) => {
	try {
		const gateway = getGateway();
		const response = await gateway.clientToken.generate({});
		return res.status(ACCEPTED).json(response.clientToken);
	} catch (error) {
		return res.status(INTERNAL_SERVER_ERROR).json({
			message: error.message
		});
	}
});

braintreeRouter.post('/sale', async (req, res) => {
	const price = req.body.itemInfo.price;
	const nonce = req.body.payload.nonce;
	const gateway = getGateway();
	try {
		const result = await gateway.transaction.sale({
			amount: price,
			paymentMethodNonce: nonce,
			options: {
				submitForSettlement: true
			}
		});
		return res.status(ACCEPTED).json(result);
	} catch (error) {
		return res.status(INTERNAL_SERVER_ERROR).json({
			message: error
		});
	}
});

export default braintreeRouter;
