export class ShopItemModel {
  public name: string;
  public price: number;
  public description: string;
  public image: string;
}
