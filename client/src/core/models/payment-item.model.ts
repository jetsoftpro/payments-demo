import { PaymentsTabType } from 'src/enum/modal-tab.enum';

export class PaymentItemModel {
  public name: string;
  public type: PaymentsTabType;
  public image: string;
}
