export enum PaymentsTabType {
  payments,
  paypal,
  googlepay,
  stripe,
  braintree,
  coinbase,
  square
}
