import { PaymentsTabType } from 'src/enum/modal-tab.enum';
import { PaymentItemModel } from 'src/core/models/payment-item.model';

const paymentSystems: PaymentItemModel[] = [
  {
    name: 'PayPal',
    type: PaymentsTabType.paypal,
    image: 'assets/images/payment-systems/PayPal-Logo.png'
  },
  {
    name: 'Google Pay',
    type: PaymentsTabType.googlepay,
    image: 'assets/images/payment-systems/google-pay-btn.png'
  },
  {
    name: 'Stripe',
    type: PaymentsTabType.stripe,
    image: 'assets/images/payment-systems/stripe-logo.png'
  },
  {
    name: 'BrainTree',
    type: PaymentsTabType.braintree,
    image: 'assets/images/payment-systems/braintree-logo.png'
  },
  {
    name: 'Coinbase',
    type: PaymentsTabType.coinbase,
    image: 'assets/images/payment-systems/coinbase-logo.png'
  },
  {
    name: 'Square',
    type: PaymentsTabType.square,
    image: 'assets/images/payment-systems/square-logo.svg'
  }
];

export default paymentSystems;
