import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShopItemListComponent } from './shop-item-list/shop-item-list.component';
import { PaymentsModule } from './payments/payments.module';
import { BraintreeService } from './payments/braintree/braintree.service';

@NgModule({
  declarations: [
    AppComponent,
    ShopItemListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    PaymentsModule,
    HttpClientModule
  ],
  providers: [
    BraintreeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
