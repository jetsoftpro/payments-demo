import { Component } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ShopItemModel } from 'src/core/models/shop-item.model';
import ShopItemsData from '../../assets/data/shop-items.json';
import { PaymentsComponent } from '../payments/payments.component';

@Component({
  selector: 'app-shop-item-list',
  templateUrl: './shop-item-list.component.html',
  styleUrls: ['./shop-item-list.component.scss']
})
export class ShopItemListComponent {

  public items: ShopItemModel[] = ShopItemsData;

  public constructor(private modalService: NgbModal) { }

  public open(itemInfo: ShopItemModel): void {
    const modalRef: NgbModalRef = this.modalService.open(PaymentsComponent, { size: 'xl', centered: true });
    modalRef.componentInstance.itemInfo = itemInfo;
  }

}
