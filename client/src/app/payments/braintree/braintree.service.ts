import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class BraintreeService {

  public constructor(private _httpClient: HttpClient) { }

  public getToken(): Observable<string> {
    return this._httpClient.get<string>(`${environment.api}/braintree/client_token`);
  }

  public onSale(paymentInfo: any): Observable<Object> {
    return this._httpClient.post<Object>(`${environment.api}/braintree/sale`, {
      ...paymentInfo
    });
  }

}
