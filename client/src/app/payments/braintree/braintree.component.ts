import { Component, Input, OnInit } from '@angular/core';

import * as dropin from 'braintree-web-drop-in';
import { ToastrService } from 'ngx-toastr';

import { ShopItemModel } from 'src/core/models/shop-item.model';
import { BraintreeService } from './braintree.service';

@Component({
  selector: 'app-braintree',
  templateUrl: './braintree.component.html',
  styleUrls: ['./braintree.component.scss']
})
export class BraintreeComponent implements OnInit {

  @Input() public item: ShopItemModel;

  public dropinInstance: any;
  public isLoading: boolean = true;

  public emailValue: string;
  public phoneValue: string;
  public nameValue: string;
  public surnameValue: string;
  public streetAdressValue: string;
  public extendAdressValue: string;
  public localityValue: string;
  public regionValue: string;
  public postalCodeValue: string;
  public countryCodeValue: string;

  public constructor(
    private _braintreeService: BraintreeService,
    private _toasterService: ToastrService
  ) { }

  public ngOnInit(): void {
    this.autofill();
    this._braintreeService.getToken().subscribe((token: string) => {
      this.setupDropin(token).then((instance: any) => this.dropinInstance = instance);
      return token;
    });
  }

  public setupDropin(clientToken: string): Promise<void> {
    this.isLoading = false;
    return dropin.create({
      authorization: clientToken,
      container: '#drop-in',
      threeDSecure: true
    });
  }

  public autofill(): void {
    this.emailValue = 'your.email@email.com';
    this.phoneValue = '123-456-7890';
    this.nameValue = 'Jane';
    this.surnameValue = 'Doe';
    this.streetAdressValue = '123 XYZ Street';
    this.localityValue = 'Anytown';
    this.regionValue = 'IL';
    this.postalCodeValue = '12345';
    this.countryCodeValue = 'US';
  }

  public onSubmit(): void {
    this.isLoading = true;
    this.dropinInstance.requestPaymentMethod({
      threeDSecure: {
        amount: this.item.price,
        email: this.emailValue,
        billingAddress: {
          givenName: this.nameValue,
          surname: this.surnameValue,
          phoneNumber: this.phoneValue.replace(/[\(\)\s\-]/g, ''),
          streetAddress: this.streetAdressValue,
          extendedAddress: this.extendAdressValue,
          locality: this.localityValue,
          region: this.regionValue,
          postalCode: this.postalCodeValue,
          countryCodeAlpha2: this.countryCodeValue
        }
      }
    }, (err: Error, payload: any) => {
      if (err) {
        this.isLoading = false;
        console.log('tokenization error:');
        console.log(err);
        this.dropinInstance.clearSelectedPaymentMethod();
        return;
      }

      if (!payload.liabilityShifted) {
        this.isLoading = false;
        console.log('Liability did not shift', payload);
        this._toasterService.error('Error');
        return;
      }

      this.isLoading = false;
      const data: any = {
        payload: payload,
        itemInfo: this.item
      };
      this._braintreeService.onSale(data).subscribe(
        (resp: any) => {
          resp.success ?
            this._toasterService.success(`Price: ${resp.transaction.amount}$`, 'Success') :
            console.log(resp);
        },
        ({ error }: { error: Error }) => this._toasterService.error(error.message, 'Error')
      );
    });
  }

}
