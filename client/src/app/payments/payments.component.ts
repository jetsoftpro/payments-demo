import { Component, Input } from '@angular/core';

import { ShopItemModel } from 'src/core/models/shop-item.model';
import { PaymentsTabType } from '../../enum/modal-tab.enum';
import paymentSystems from '../../assets/data/payment-systems-data';
import { PaymentItemModel } from 'src/core/models/payment-item.model';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent {

  @Input() public itemInfo: ShopItemModel;

  public paymentSystems: PaymentItemModel[] = paymentSystems;

  public tabType: typeof PaymentsTabType = PaymentsTabType;

  public currentTab: PaymentsTabType = PaymentsTabType.payments;

}
