import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, Input, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ShopItemModel } from '../../../core/models/shop-item.model';
import { environment } from '../../../environments/environment';

declare const SqPaymentForm: any;

@Component({
  selector: 'app-square',
  templateUrl: 'square.component.html',
  styleUrls: ['square.component.scss']
})
export class SquareComponent implements OnInit {

  public givenName: string;
  public familyName: string;
  public loading: boolean = true;
  public email: string;

  public paymentForm: {
    requestCardNonce: Function,
    build: Function,
    verifyBuyer: Function
  };
  @Input() public item: ShopItemModel;

  public constructor(
    @Inject(DOCUMENT) private _document: Document,
    private _toasterService: ToastrService,
    private _httpClient: HttpClient,
    private _renderer2: Renderer2
  ) { }

  public loadScript(url: string): Promise<void> {
    return new Promise((resolve: Function, reject: Function) => {
      const script: HTMLScriptElement = this._renderer2.createElement('script');
      script.src = url;
      script.onload = <any>resolve;
      script.onerror = <any>reject;
      this._renderer2.appendChild(this._document.body, script);
    });
  }

  public async ngOnInit(): Promise<void> {
    if (environment.production) {
      await this.loadScript('https://js.squareup.com/v2/paymentform');
    } else {
      await this.loadScript('https://js.squareupsandbox.com/v2/paymentform');
    }
    this.paymentForm = new SqPaymentForm({
      // Initialize the payment form elements
      applicationId: environment.squareConfig.applicationId,
      locationId: environment.squareConfig.locationId,
      inputClass: 'sq-input',
      autoBuild: false,
      // Customize the CSS for SqPaymentForm iframe elements
      inputStyles: [{
        fontSize: '16px',
        lineHeight: '24px',
        padding: '16px',
        placeholderColor: '#a0a0a0',
        backgroundColor: 'transparent',
      }],
      // Initialize the credit card placeholders
      cardNumber: {
        elementId: 'sq-card-number',
        placeholder: 'Card Number'
      },
      cvv: {
        elementId: 'sq-cvv',
        placeholder: 'CVV'
      },
      expirationDate: {
        elementId: 'sq-expiration-date',
        placeholder: 'MM/YY'
      },
      postalCode: {
        elementId: 'sq-postal-code',
        placeholder: 'Postal'
      },
      // SqPaymentForm callback functions
      callbacks: {
        paymentFormLoaded: () => {
          this.loading = false;
        },
        /*
        * callback function: cardNonceResponseReceived
        * Triggered when: SqPaymentForm completes a card nonce request
        */
        cardNonceResponseReceived: (errors: Array<Error>, nonce: any, cardData: any) => {
          if (errors) {
            this.loading = false;
            // Log errors from nonce generation to the browser developer console.
            console.error('Encountered errors:');
            errors.forEach((error: Error) => {
              console.error('  ' + error.message);
            });
            console.log('Encountered errors, check browser developer console for more details');
            return;
          }
          console.log(`The generated nonce is:\n${nonce}`);
          const verificationDetails: any = {
            intent: 'CHARGE',
            amount: this.item.price.toFixed(2),
            currencyCode: 'USD',
            billingContact: {
              givenName: this.givenName,
              familyName: this.familyName,
              email: this.email
            }
          };
          this.paymentForm.verifyBuyer(
            nonce,
            verificationDetails,
            async (error: Error, verificationResult: { token: string }) => {
              console.log(verificationResult);
              if (!error) {
                try {
                  const response: any = await this._httpClient.post(`${environment.api}/square/charge`, {
                    nonce: nonce,
                    token: verificationResult.token,
                    price: this.item.price
                  })
                    .toPromise();
                  console.log(response);
                  this._toasterService.success('Success', 'Done');
                  this.loading = false;
                } catch ({ error }) {
                  this.loading = false;
                  console.error('  ' + error.message);
                }
              } else {
                this.loading = false;
                console.error('  ' + error.message);
              }
            }
          );
        }
      }
    });
    this.paymentForm.build();
  }

  public onGetCardNonce(event: Event): void {
    event.preventDefault();
    this.loading = true;
    this.paymentForm.requestCardNonce();
  }

}
