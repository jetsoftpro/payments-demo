import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ElementOptions, ElementsOptions, Error as StripeError, PaymentIntentResult, SourceResult, StripeCardComponent, StripeService } from 'ngx-stripe';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss']
})
export class StripeComponent implements OnInit {

  @ViewChild(StripeCardComponent) public card: StripeCardComponent;

  @Input() public price: number;

  public stripeForm: FormGroup;
  public loading: boolean = false;
  public error: Error | StripeError;

  public cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#111',
        color: '#111',
        fontSize: '16px',
        '::placeholder': {
          color: '#111'
        }
      }
    }
  };
  public elementsOptions: ElementsOptions = {
    locale: 'auto'
  };

  public constructor(
    private _stripeService: StripeService,
    private _formBuilder: FormBuilder,
    private _httpClient: HttpClient
  ) {}

  public get name(): AbstractControl {
    return this.stripeForm.get('name');
  }

  public get email(): AbstractControl {
    return this.stripeForm.get('email');
  }

  public ngOnInit(): void {
    this.stripeForm = this._formBuilder.group({
      name: this._formBuilder.control(undefined, [Validators.required]),
      email: this._formBuilder.control(undefined, [Validators.required, Validators.email])
    });
  }

  public submitStripeForm(): void {
    this.error = null;
    this.loading = true;
    const { name, email }: { name: string, email: string } = this.stripeForm.value;
    /**
     * @docs {@link https://stripe.com/docs/js/tokens_sources/create_source}
     */
    this._stripeService.createSource(this.card.getCard(), { owner: { name: name, email: email } })
      .subscribe(({ error, source }: SourceResult) => {
        if (!error) {
          this._httpClient.post(`${environment.api}/stripe/charge`, { source: source.id, price: this.price })
            .subscribe(({ secret }: { secret: string }) => {
              /**
               * @docs {@link https://stripe.com/docs/js/deprecated/handle_card_payment}
               */
              this._stripeService.handleCardPayment(secret, this.card.element)
                .subscribe(({ error, paymentIntent }: PaymentIntentResult) => {
                  if (!error) {
                    console.log(paymentIntent);
                    this.loading = false;
                  } else {
                    this.loading = false;
                    this.error = error;
                  }
                });
            }, ({ error }: { error: Error }) => {
              this.loading = false;
              this.error = error;
            });
        } else {
          this.loading = false;
          this.error = error;
        }
      });
  }
}
