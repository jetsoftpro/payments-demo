import { HttpClient } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ShopItemModel } from '../../../core/models/shop-item.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-coinbase',
  templateUrl: './coinbase.component.html',
  styleUrls: ['./coinbase.component.scss']
})
export class CoinbaseComponent implements OnInit, OnDestroy {

  @Input() public item: ShopItemModel;
  public paymentUrl: SafeResourceUrl = '';
  public showPaymentModal: boolean = false;
  public status: string = '';
  public error: Error;

  private _frameMessagesFn: EventListenerOrEventListenerObject = this.onFrameMessages.bind(this);

  public constructor(private _httpClient: HttpClient, private _domSanitizer: DomSanitizer) { }

  public ngOnInit(): void {
    window.addEventListener('message', this._frameMessagesFn);
    this.getChargeUrl()
      .subscribe(
        (chargeUrl: string) => {
          this.paymentUrl = chargeUrl;
        },
        ({ error }: { error: Error }) => {
          this.error = error;
        });
  }

  public ngOnDestroy(): void {
    window.removeEventListener('message', this._frameMessagesFn);
  }

  public getChargeUrl(): Observable<SafeResourceUrl> {
    return this._httpClient.post(`${environment.api}/coinbase/charge`, this.item)
      .pipe(
        map((response: any) => {
            console.log(response);
            return this._domSanitizer.bypassSecurityTrustResourceUrl(`https://commerce.coinbase.com/embed/charges/${response.code}?version=201807&origin=${location.origin}`);
          }
        )
      );
  }

  public onFrameMessages({ data, origin }: { data: { event: string }, origin: string }): void {
    if (origin === 'https://commerce.coinbase.com') {
      console.log(data.event);
      this.status = data.event;
      switch (data.event) {
        case 'charge_confirmed':
          // @todo COINBASE: charge confirm
          break;
        case 'charge_failed':
          // @todo COINBASE: charge failed
          break;
        case 'payment_detected':
          // @todo COINBASE: payment detected
          break;
        case 'error_not_found':
          // @todo COINBASE: charge confirm
          break;
        case 'checkout_modal_loaded':
          // @todo COINBASE: charge confirm
          break;
        case 'checkout_modal_closed':
          this.showPaymentModal = false;
      }
    }
  }

  public onPaymentButtonClick(): void {
    this.showPaymentModal = true;
  }
}
