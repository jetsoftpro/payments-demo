import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GooglepayComponent } from './googlepay/googlepay.component';
import { PaymentsComponent } from './payments.component';
import { PaypalComponent } from './paypal/paypal.component';
import { SquareComponent } from './square/square.component';
import { StripeComponent } from './stripe/stripe.component';
import { NgxStripeModule } from 'ngx-stripe';
import { environment } from 'src/environments/environment';
import { BraintreeComponent } from './braintree/braintree.component';
import { BraintreeService } from './braintree/braintree.service';
import { CoinbaseComponent } from './coinbase/coinbase.component';

@NgModule({
  declarations: [
    PaymentsComponent,
    PaypalComponent,
    GooglepayComponent,
    StripeComponent,
    BraintreeComponent,
    CoinbaseComponent,
    SquareComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgxStripeModule.forRoot(environment.stripeConfig.key)
  ],
  providers: [
    BraintreeService
  ]
})
export class PaymentsModule {}
