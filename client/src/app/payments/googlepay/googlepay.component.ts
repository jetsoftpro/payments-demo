import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

/**
 * Implementation Google Pay API
 * @docs https://developers.google.com/pay/api/web/guides/tutorial#apiversion
 */
@Component({
  selector: 'app-googlepay',
  templateUrl: './googlepay.component.html',
  styleUrls: ['./googlepay.component.scss']
})
export class GooglepayComponent implements OnInit {

  @Input() public price: number;
  @ViewChild('payButton', { static: true }) public buttonContainer: ElementRef;
  public paymentStatus: string = '';
  public paymentsClient: google.payments.api.PaymentsClient;

  /**
   * @docs {@link https://developers.google.com/pay/api/web/reference/request-objects#PaymentOptions}
   */
  private readonly _paymentOptions: google.payments.api.PaymentOptions = {
    environment: 'TEST',
    merchantInfo: {
      ...environment.googlePayConfig
    },
    paymentDataCallbacks: {
      onPaymentAuthorized: this._onPaymentAuthorized.bind(this)
    }
  };

  /**
   * @docs {@link https://developers.google.com/pay/api/web/reference/request-objects#IsReadyToPayRequest }
   */
  private readonly _basePaymentRequest: google.payments.api.IsReadyToPayRequest = {
    apiVersion: 2,
    apiVersionMinor: 0,
    allowedPaymentMethods: [
      {
        type: 'CARD',
        parameters: {
          allowedAuthMethods: ['PAN_ONLY', 'CRYPTOGRAM_3DS'],
          allowedCardNetworks: ['AMEX', 'DISCOVER', 'INTERAC', 'JCB', 'MASTERCARD', 'VISA']
        },
        tokenizationSpecification: {
          type: 'PAYMENT_GATEWAY',
          parameters: {
            gateway: 'example',
            gatewayMerchantId: 'exampleGatewayMerchantId'
          }
        }
      }
    ]
  };

  public constructor(private _httpClient: HttpClient) { }

  public async ngOnInit(): Promise<void> {
    await this._initPaymentClient();
  }

  private async _initPaymentClient(): Promise<void> {
    this.paymentsClient = new google.payments.api.PaymentsClient(this._paymentOptions);
    try {
      // Determine readiness to pay with the Google Pay API
      if ((await this.paymentsClient.isReadyToPay(this._basePaymentRequest)).result) {
        // Add a Google Pay payment button
        this._createPaymentButton();
      }

    } catch (err) {
      throw new Error(err);
    }
  }

  private _createPaymentButton(): void {
    const button: HTMLElement = this.paymentsClient.createButton({ onClick: this._onPaymentButtonClick.bind(this) });
    this.buttonContainer.nativeElement.appendChild(button);
  }

  private async _onPaymentButtonClick(): Promise<void> {
    const paymentDataRequest: any = await this._getPaymentDataRequest(this.price);
    /**
     * @docs {@link https://developers.google.com/pay/api/web/guides/tutorial#register-onpaymentauthorized-callbacks}
     */
    paymentDataRequest.callbackIntents = ['PAYMENT_AUTHORIZATION'];
    try {
      const paymentData: google.payments.api.PaymentData = await this.paymentsClient.loadPaymentData(paymentDataRequest);
      this.paymentStatus = 'SUCCESS';
    } catch (err) {
      if (err.hasOwnProperty('statusCode')) {
        this.paymentStatus = err.statusCode;
      }
      console.error(err);
    }

  }

  private async _getPaymentDataRequest(price: number): Promise<google.payments.api.PaymentDataRequest> {
    const paymentDataRequest: any = Object.assign({}, this._basePaymentRequest);
    paymentDataRequest.transactionInfo = {
      totalPriceStatus: 'FINAL',
      totalPrice: price.toString(),
      currencyCode: 'USD',
      countryCode: (<{ countryCode: string }>(await this._httpClient.get('http://ip-api.com/json').toPromise())).countryCode
    };

    return paymentDataRequest;
  }

  /**
   * @docs {@link https://developers.google.com/pay/api/web/guides/tutorial#handle-onpaymentauthorized-callbacacks}
   */
  private _onPaymentAuthorized(paymentData: google.payments.api.PaymentData): Promise<google.payments.api.PaymentAuthorizationResult> {
    /**
     * @docs {@link https://developers.google.com/pay/api/web/reference/response-objects#PaymentAuthorizationResult}
     */
    return this._httpClient.post(`${environment.api}/googlepay/charge`, paymentData)
      .pipe(
        map((response: any) => {
          if (response.status === 'SUCCESS') {
            return {
              transactionState: response.status
            } as google.payments.api.PaymentAuthorizationResult;
          }
          return {
            transactionState: 'ERROR',
            error: {
              intent: 'PAYMENT_AUTHORIZATION',
              message: '{{PAYMENT_DATA_INVALID}}',
              reason: 'PAYMENT_DATA_INVALID'
            }
          } as google.payments.api.PaymentAuthorizationResult;
        }),
        catchError(({ error }: { error: Error }) => {
          return of({
            transactionState: 'ERROR',
            error: {
              intent: 'PAYMENT_AUTHORIZATION',
              message: error.message,
              reason: 'OTHER_ERROR'
            }
          } as google.payments.api.PaymentAuthorizationResult);
        })
      )
      .toPromise();
  }

}
