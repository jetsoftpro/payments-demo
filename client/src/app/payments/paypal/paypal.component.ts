import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

declare let paypal: any;

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.scss']
})
export class PaypalComponent implements OnInit {

  @Input() public price: number;
  public paypalConfig: any = {
    env: 'sandbox',
    client: {
      sandbox: environment.paypalConfig.client.sandbox,
      production: environment.paypalConfig.client.production
    },
    style: {
      color: 'blue',
      shape: 'pill',
      label: 'pay',
      height: 40
    },
    commit: true,
    payment: (data: any, actions: any) => {
      return actions.payment.create({
        payment: {
          transactions: [
            { amount: { total: this.price, currency: 'USD' } }
          ]
        }
      });
    },
    onAuthorize: (data: any, actions: any) => {
      actions.payment.execute()
        .then((payment: any) => {
          console.log(payment);
          this._toaster.success('success', payment);
        });
    },
    onError: (error: Error) => this._toaster.error('failed', error.message)
  };

  public constructor(private _toaster: ToastrService) {}

  public ngOnInit(): void {
    paypal.Button.render(this.paypalConfig, '#paypal-button-container');
  }

}
