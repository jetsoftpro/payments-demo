// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: any = {
  production: true,
  api: 'http://192.168.87.123:3000',
  paypalConfig: {
    env: 'sandbox',
    client: {
      sandbox: 'Aap6VRUOmMA4jgbpulV0MK_lnyadbheGB9dyxcqXlJAHR-XErYRdxXaq1VpkA4ZkPAwJDUC_fCLofvHn',
      production: 'Aap6VRUOmMA4jgbpulV0MK_lnyadbheGB9dyxcqXlJAHR-XErYRdxXaq1VpkA4ZkPAwJDUC_fCLofvHn'
    }
  },
  stripeConfig: {
    key: 'pk_test_EbjvPtmJ9dMMLd6zfn6GJs5M00rxujhzck'
  },
  braintreeConfig: {
    key: 'sandbox_jy4dv9f3_rkkz429hvym2c6ws'
  },
  googlePayConfig: {
    merchantName: 'Example Merchant',
    merchantId: '0123456789012345678901234567890123456789'
  },
  squareConfig: {
    applicationId: 'sandbox-sq0idb-dPa3DG7cdSo05HNAfpQqgw',
    locationId: '5RDMKV0AW5E0X'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
